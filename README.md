# Open Market

A silver-standard price list for fantasy RPGs with city & rural prices. Prepared for earliest rules, but should be compatible with many systems.

## Contents

 * README.md -- This file.
 * MARKET.md -- The full price list in Markdown. Content of MARKET.md is Open Game Content under the Open Game License (see LICENSE.md).
 * LICENSE.md -- The Open Game Content declaration & Open Game License 1.0a.

## Usage

A document processor such as [pandoc](http://pandoc.org) may be used to process the market markdown file into HTML or other formats.

For example, with pandoc installed the following commands can be used to generate usable monolithic HTML:

```
cat MARKET.md LICENSE.md | pandoc --toc -s -H "extra/head.html" -o open-market.html
```

OpenDocument text:

```
cat MARKET.md LICENSE.md | pandoc -o open-market.odt
```

or even PDF (requires [LaTeX](https://www.latex-project.org/)):

```
cat MARKET.md LICENSE.md | pandoc --toc -o open-market.pdf
```

## Rules Notes

This price list was prepared for use in a silver-standard economy. It was prepared for use with game rules based on the earliest traditions, such that all weapons deal 1d6 damage in normal combat. Thus, damage values for weapons are not given.

## Find a use for this?

If you use this multi-format, OGC, silver-standard-based equipment price list, please consider [dropping a tip in the jar on ko-fi](http://ko-fi.com/acodispo)!
